from flask import Flask, render_template, redirect, url_for, request
#import os
#from flask_sqlalchemy import SQLAlchemy
import psycopg2
from flask import send_file
import argparse

parser = argparse.ArgumentParser(description='Process dbhost and dbport parameters.')

parser.add_argument("dbhost")
parser.add_argument("dbport")

args = parser.parse_args()

#os.chdir(r"C:\Users\achernaev\Desktop\Diplomna\PythonProjectDiplomna\Diplomna\GitlabRepo\diplomawork_antonchernaev")
#os.getcwd()
#from Flask import flask
connection = psycopg2.connect(user = "anton",
                                 password = "anton",
                                 host = args.dbhost,
                                 port = args.dbport,
                                 database = "users")
cur = connection.cursor()
cur.execute("SELECT * FROM accounts;")
credAuth = cur.fetchall()
connection.close()
dbDictCred = {}
for x in range(len(credAuth)):
    line = (credAuth[x])
    user = (line[1])
    password = (line[2])
    dbDictCred.update({user: password})

app = Flask(__name__)

@app.route('/show')
def show_image():
     return send_file('Scheme.png', mimetype='image/png') 

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        #if request.form['username'] != 'admin' or request.form['password'] != 'admin':
        if request.form['username'] not in dbDictCred.keys() or request.form['password'] != dbDictCred[request.form['username']]:
            error = 'Invalid Credentials. Please try again.'
        else:
           return redirect(url_for('show_image'))
    return render_template('login.html', error=error)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=999)
