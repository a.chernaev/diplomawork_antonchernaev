node {
    
    stage 'Run Terraform'
    
    dir ('diplomawork_antonchernaev/Terraform'){
        sh '''
        git pull
        terraform init
        terraform plan
        terraform apply
        '''
    }
        
    
    stage 'Run Selenium'
    
    dir ('diplomawork_antonchernaev'){
            sh '''
            git pull
            Xvfb :99 -ac &
            export DISPLAY=:99 
            chromedriver &
            python3 SeleniumTest.py
            '''
    }
}