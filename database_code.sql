psql
\connect users

CREATE TABLE accounts (
	user_id serial PRIMARY KEY,
	username VARCHAR ( 50 ) UNIQUE NOT NULL,
	password VARCHAR ( 50 ) NOT NULL,
	email VARCHAR ( 255 ) UNIQUE NOT NULL,
	created_on TIMESTAMP NOT NULL,
    last_login TIMESTAMP 
);

INSERT INTO accounts (user_id, username, password, email, created_on)
VALUES ('1', 'achernaev', 'test', 'achernaev@abv.bg', '08.09.2020');
INSERT INTO accounts (user_id, username, password, email, created_on)
VALUES ('2', 'ggeorgiev', 'FMI', 'achernaev@fmi.bg', '08.09.2020');
RETURNING username;