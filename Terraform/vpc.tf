variable "project_id" {
  default = "invertible-hook-283719"
  description = "project id"
}

variable "region_vpc" {
  default = "us-central1"
  description = "region"
}

#provider "google" {
#  project = var.project_id
#  region  = var.region
#}

# VPC
resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  auto_create_subnetworks = "false"
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.project_id}-subnet"
  region        = var.region_vpc
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.10.0.0/24"

}

output "region" {
  value       = var.region_vpc
  description = "region"
}