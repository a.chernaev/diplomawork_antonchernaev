provider "google" {
  #credentials = file("C:\Users\achernaev\AppData\Roaming\gcloud\application_default_credentials.json")
  project     = "invertible-hook-283719"
  region      = "us-central1-a"
}

# Create container registry
resource "google_container_registry" "registry" {
  project  = "invertible-hook-283719"
  location = "EU"
}

# GKE cluster variables

variable "gke_username" {
  default     = "anton"
  description = "gke username"
}

variable "gke_password" {
  default     = "testTestAnton1234"
  description = "gke password"
}

variable "gke_num_nodes" {
  default     = 1
  description = "number of gke nodes"
}

variable "region" {
  default = "us-central1-a"
  description = "region"
}

# GKE cluster
resource "google_container_cluster" "primary" {
  name     = "diploma-gke"
  location = "us-central1-a"

  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  master_auth {
    username = var.gke_username
    password = var.gke_password

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  depends_on = [google_container_cluster.primary]
  name       = "${google_container_cluster.primary.name}-node-pool"
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = var.gke_num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project_id
    }

    # preemptible  = true
    machine_type = "n1-standard-2"
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}


provider "kubernetes" {
  load_config_file = false
  host = "https://${google_container_cluster.primary.endpoint}"
  #cluster_ca_certificate = "${base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)}"
  #token = "${data.google_client_config.current.access_token}"
  username = "anton"
  password = "testTestAnton1234"
  insecure = true # Do not verify tls connection
  #load_config_file = false 
}

resource "kubernetes_service" "db" {
  metadata {
    name = "database"
  }
  spec {
    selector = {
      db = "${kubernetes_pod.db.metadata.0.labels.db}"
    }
    session_affinity = "ClientIP"
    port {
      port        = 5432
      target_port = 5432
    }

    type = "LoadBalancer"
  }
}


resource "kubernetes_pod" "db" {
  metadata {
    name = "database"
    labels = {
      db = "database"
    }
  }

  spec {
    container {
      image = "eu.gcr.io/invertible-hook-283719/pg_container:latest"
      name  = "pg-container"
    }
  }
}

resource "kubernetes_service" "app" {
  metadata {
    name = "python-app"
  }
  spec {
    selector = {
      app = "${kubernetes_pod.app.metadata.0.labels.app}"
    }
    session_affinity = "ClientIP"
    port {
      port        = 80
      target_port = 999
    }

    type = "LoadBalancer"
  }
}

resource "kubernetes_pod" "app" {
  metadata {
    name = "app"
    labels = {
      app = "python-app"
    }
  }

  spec {
    container {
      image = "eu.gcr.io/invertible-hook-283719/python_app"
      name  = "app"
      #command = ["ping", "localhost"]
      command = ["python3", "/PythonApp/WebPageDiplomna.py", "${data.kubernetes_service.db.load_balancer_ingress[0].ip}", "${data.kubernetes_service.db.spec[0].port[0].port}"]
    }
  }
}



#resource "kubernetes_deployment" "db" {
#  depends_on = [google_container_node_pool.primary_nodes]
#  metadata {
#    name = "database"
#    labels = {
#      container = "db"
#    }
#  }
#
#  spec {
#    replicas = 1
#
#    selector {
#      match_labels = {
#        container = "db"
#      }
#    }
#
#    template {
#      metadata {
#        labels = {
#          container = "db"
#        }
#      }
#
#      spec {
#        container {
#          image = "eu.gcr.io/invertible-hook-283719/pg_container:latest"
#          name  = "pg-container"
#        }
#      }
#    }
#  }
#}

#resource "kubernetes_service" "db" {
#
#  metadata {
#    name = "database"
#  }
#  spec {
#    selector = {
#      Database = kubernetes_deployment.db.metadata[0].labels.container
#    }
#    port {
#      port        = 5432
#      target_port = 5432
#    }
#    type = "LoadBalancer"
#    #type = "ClusterIP"
#  }
#  depends_on = [kubernetes_deployment.db]
#}

data "kubernetes_service" "db" {
  metadata {
    name = "database"
  }
  depends_on = [kubernetes_service.db]
}

output "kubernetes_database_service_ip" {
  value       = data.kubernetes_service.db.load_balancer_ingress[0].ip
  #data.kubernetes_service.db.spec[0].cluster_ip
  description = "GKE Cluster Name"
  depends_on = [kubernetes_service.db]
}

output "kubernetes_database_service_port" {
  value       = data.kubernetes_service.db.spec[0].port[0].port
  description = "GKE Cluster Name"
  depends_on = [kubernetes_service.db]
}
#resource "kubernetes_deployment" "app" {
#  depends_on = [kubernetes_service.db]
#  metadata {
#    name = "app"
#    labels = {
#      container = "app"
#    }
#  }
#
#  spec {
#    replicas = 1
#
#    selector {
#      match_labels = {
#        container = "app"
#      }
#    }
#
#    template {
#      metadata {
#        labels = {
#          container = "app"
#        }
#      }
#
#      spec {
#        container {
#          image = "eu.gcr.io/invertible-hook-283719/python_app"
#          name  = "app-container"
#          #command = ["python3 WebPageDiplomna.py ${data.kubernetes_service.db.spec[0].cluster_ip} ${data.kubernetes_service.db.spec[0].port[0].port}"]
#          command = ["python3 /PythonApp/WebPageDiplomna.py 10.27.241.211 5432"]
#        }
#      }
#    }
#  }
#}